# pylint: disable=import-error
from api.models.User import User
from api.config import Config
from flask import Blueprint, request, jsonify
import jwt

token_login_user = Blueprint('token_login_user', __name__)

@token_login_user.route('/', methods=['POST'])
def home():
  try:
    obj = jwt.decode(request.json['token'], Config.SECRET_KEY, algorithms=["HS256"])
    return jsonify(obj), 200
  except Exception as e:
    return jsonify({
      "message": str(e)
    }), 400