# pylint: disable=import-error
from api.models.User import User
from flask import Blueprint, jsonify

get_users = Blueprint('get_users', __name__)

@get_users.route('/')
def home():
  return jsonify(User.get_all()), 200