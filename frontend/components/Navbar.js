import { useRouter } from 'next/router';
import { useState } from 'react';

const Navbar = ({ page, setIsLogin, user, isUpdate, setIsUpdate }) => {
  const router = useRouter();
  const [localIsUpdate, setLocalIsUpdate] = useState(false);
  
  const signOut = () => {
    if (typeof window !== "undefined")
    {
      localStorage.removeItem('token');
      router.push('/');
    }
  };

  return (
    <nav>
      <ul>
        {page === '/' ?
        <>
          <li>Simple Web Api</li>
          <li onClick={() => setIsLogin(false)}>Sign Up</li>
          <li onClick={() => setIsLogin(true)}>Login</li>
        </> :
        <>
          <li>{`Welcome ${user.name}`}</li>
          <li onClick={() => { setIsUpdate(!localIsUpdate);setLocalIsUpdate(!localIsUpdate); }}>
            {!localIsUpdate ? `Edit Profile` : `Back to Home`}
          </li>
          <li onClick={signOut}>Sign Out</li>
        </>
        }
      </ul>
    </nav>
  );
}

export default Navbar;
