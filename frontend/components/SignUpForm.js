import { useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';

const SignUpForm = ({ page, user, setUser }) => {
  const router = useRouter();

  const [state, setstate] = useState({
    name: page==="/" ? '' : user.name,
    email: page==="/" ? '' : user.email,
    password: page==="/" ? '' : '',
    phone: page==="/" ? '' : user.phone
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setstate({...state, [name]: value});
  }

  const handleClick = () => {
    if (page === "/")
      axios.post('http://localhost:5000/api/create_user/', state)
        .then(res => {
          if (res.status == 200)
            console.log(res.data);
        })
        .catch(err => console.log(err));
    else
    {
      axios.patch(`http://localhost:5000/api/update_user/${user.id}/`, state)
      .then(res => {
        if (res.status == 200)
          if (typeof window !== "undefined")
          {
            localStorage.setItem('user', JSON.stringify(res.data));
            setUser(res.data);
            router.push('/home');
          }
      })
      .catch(err => console.log(err));
    }
  }

  return (
    <div className="login-signup-form">
      {page==="/" ?
        <h2>Sign Up</h2> :
        <h2>Update Profile</h2>
      }
      <label>
        Name:
        <input type="text" name="name" value={state.name} onChange={handleChange} />
      </label>
      <label>
        Email:
        <input type="email" name="email" value={state.email} onChange={handleChange} />
      </label>
      {page==="/" ?
        <label>
          Password:
          <input type="password" name="password" value={state.password} onChange={handleChange} />
        </label> :
        null
      }
      <label>
        Phone:
        <input type="text" name="phone" value={state.phone} onChange={handleChange} />
      </label>
      <button onClick={handleClick}>Submit</button>
    </div>
  );
}

export default SignUpForm;
