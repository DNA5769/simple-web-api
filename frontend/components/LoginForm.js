import { useRouter } from 'next/router';
import { useState } from 'react';
import axios from 'axios';

const LoginForm = () => {
  const router = useRouter();

  const [state, setstate] = useState({
    email: '',
    password: ''
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setstate({...state, [name]: value});
  }

  const handleClick = () => {
    axios.post('http://localhost:5000/api/login_user/', state)
      .then(res => {
        if (res.status == 200)
        {
          if (typeof window !== "undefined")
          {
            localStorage.setItem('token', res.data);
            router.push('/home');
          }
        }
      })
      .catch(err => console.log(err));
  }

  return (
    <div className="login-signup-form">
      <h2>Login</h2>
      <label>
        Email:
        <input type="email" name="email" value={state.email} onChange={handleChange} />
      </label>
      <label>
        Password:
        <input type="password" name="password" value={state.password} onChange={handleChange} />
      </label>
      <button onClick={handleClick}>Submit</button>
    </div>
  );
}

export default LoginForm;
