import Head from 'next/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';

import LoginForm from '../components/LoginForm';
import Navbar from '../components/Navbar';
import SignUpForm from '../components/SignUpForm';

const index = () => {
  const router = useRouter();
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    if (typeof window !== "undefined")
    {
      if (localStorage.getItem("token") !== null)
      {
        router.push('/home');
      }
    }
  }, []);

  return (
    <div>
      <Head>
        <title>Simple Web Api</title>
      </Head>

      <Navbar setIsLogin={setIsLogin} page="/"/>
      {isLogin ? <LoginForm /> : <SignUpForm page="/"/>}
    </div>
  );
};

export default index;
