# pylint: disable=import-error
from api.models.User import User
from flask import Blueprint, request, jsonify

create_user = Blueprint('create_user', __name__)

@create_user.route('/', methods=['POST'])
def home():
  try:
    obj = User(request.json['name'], request.json['email'], request.json['password'], request.json['phone'])
    obj.save()
    return jsonify(obj.to_json()), 200
  except Exception as e:
    return jsonify({
      "message": str(e)
    }), 400