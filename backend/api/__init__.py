from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from api.config import Config

app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

with app.app_context():
  from api.routes.get_users import get_users
  from api.routes.create_user import create_user
  from api.routes.update_user import update_user
  from api.routes.delete_user import delete_user
  from api.routes.undelete_user import undelete_user
  from api.routes.login_user import login_user
  from api.routes.token_login_user import token_login_user

app.register_blueprint(get_users, url_prefix='/api/get_users/')
app.register_blueprint(login_user, url_prefix='/api/login_user/')
app.register_blueprint(token_login_user, url_prefix='/api/token_login_user/')
app.register_blueprint(create_user, url_prefix='/api/create_user/')
app.register_blueprint(update_user, url_prefix='/api/update_user/<int:id>/')
app.register_blueprint(delete_user, url_prefix='/api/delete_user/<int:id>/')
app.register_blueprint(undelete_user, url_prefix='/api/undelete_user/<int:id>/')