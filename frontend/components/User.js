import axios from 'axios';

const User = ({ user, users, setUsers }) => {
  const handleDeactivate = () => {
    axios.delete(`http://localhost:5000/api/delete_user/${user.id}/`)
      .then(res => setUsers(users.map(u => {
        if (user.id === u.id)
        {
          let temp = JSON.parse(JSON.stringify(u));
          temp.isdeleted = !temp.isdeleted;
          return temp;
        }
        else
          return u;
      })))
      .catch(err => console.log(err));
  };

  const handleReactivate = () => {
    axios.patch(`http://localhost:5000/api/undelete_user/${user.id}/`)
      .then(res => setUsers(users.map(u => {
        if (user.id === u.id)
        {
          let temp = JSON.parse(JSON.stringify(u));
          temp.isdeleted = !temp.isdeleted;
          return temp;
        }
        else
          return u;
      })))
      .catch(err => console.log(err));
  };

  return (
    <tr>
      <td>{user.id}</td>
      <td>{user.name}</td>
      <td>{user.email}</td>
      <td>{user.phone}</td>
      <td>{!user.isdeleted ? <button onClick={handleDeactivate}>Deactivate</button> : <button onClick={handleReactivate}>Reactivate</button>}</td>
    </tr>
  );
}

export default User;
