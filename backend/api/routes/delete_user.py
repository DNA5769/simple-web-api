# pylint: disable=import-error
from api.models.User import User
from flask import Blueprint, request, jsonify

delete_user = Blueprint('delete_user', __name__)

@delete_user.route('/', methods=['DELETE'])
def home(id):
  print(request.json)
  try:
    obj = User.get(id)
    obj.delete()
    return obj.to_json(), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 400