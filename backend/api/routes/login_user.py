# pylint: disable=import-error
from api.models.User import User
from api.config import Config
from flask import Blueprint, request, jsonify
import jwt

login_user = Blueprint('login_user', __name__)

@login_user.route('/', methods=['POST'])
def home():
  try:
    obj = User.get_login(request.json['email'], request.json['password'])
    return jsonify(jwt.encode(obj.to_json(), Config.SECRET_KEY, algorithm="HS256")), 200
  except Exception as e:
    return jsonify({
      "message": str(e)
    }), 400