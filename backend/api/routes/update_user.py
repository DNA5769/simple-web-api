# pylint: disable=import-error
from api.models.User import User
from flask import Blueprint, request, jsonify

update_user = Blueprint('update_user', __name__)

@update_user.route('/', methods=['PATCH'])
def home(id):
  print(request.json)
  try:
    obj = User.get(id)
    obj.update(request.json)
    return obj.to_json(), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 400