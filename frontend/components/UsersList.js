import User from "./User";

const UsersList = ({ users, user, setUsers }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Deactivate/Reactivate</th>
        </tr>
      </thead>
      <tbody>
        {users.map(u => u.id === user.id ? null : <User key={u.id} user={u} users={users.filter(u => u.id !== user.id)} setUsers={setUsers}/>)}
      </tbody>
    </table>
  );
}

export default UsersList;
