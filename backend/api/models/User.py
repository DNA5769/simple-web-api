# pylint: disable=import-error
from api import db
from datetime import datetime
from sqlalchemy import and_

class User(db.Model):
  __tablename__ = 'users'
  id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
  name = db.Column('name', db.String(200))
  email = db.Column('email', db.String(200))
  password = db.Column('password', db.String(200))
  phone = db.Column('phone', db.Integer)
  isdeleted = db.Column('isdeleted', db.Boolean, default=lambda: False)
  createdAt = db.Column('createdAt', db.DateTime, default=datetime.utcnow)
  updatedAt = db.Column('updatedAt', db.DateTime)

  def __init__(self, name, email, password, phone):
    self.name = name
    self.email = email
    self.password = password
    self.phone = phone

  def save(self):
    db.session.add(self)
    db.session.commit()

  def delete(self):
    self.isdeleted = True
    db.session.commit()
  
  def undelete(self):
    self.isdeleted = False
    db.session.commit()

  def update(self, body):
    for x in body:
      if x == 'email':
        self.email = body[x]
        self.updatedAt = datetime.utcnow()
      elif x == 'phone':
        self.phone = body[x]
        self.updatedAt = datetime.utcnow()
      elif x == 'name':
        self.name = body[x]
        self.updatedAt = datetime.utcnow()
    db.session.commit()

  def to_json(self):
    return {
      "id": self.id,
      "name": self.name,
      "email": self.email,
      "phone": self.phone,
      "isdeleted": self.isdeleted
    }

  @classmethod
  def get(cls, id):
    return cls.query.filter(cls.id == id).first()

  @classmethod
  def get_login(cls, email, password):
    return cls.query.filter(and_(cls.email == email, cls.password == password)).first()
  
  @classmethod
  def get_all(cls):
    return [u.to_json() for u in cls.query.all()]