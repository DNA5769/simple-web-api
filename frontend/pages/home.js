import Head from 'next/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import axios from 'axios';

import Navbar from '../components/Navbar';
import UsersList from '../components/UsersList';
import SignUpForm from '../components/SignUpForm';

const index = () => {
  const router = useRouter()
  const [user, setUser] = useState(null);
  const [users, setUsers] = useState([]);
  const [isUpdate, setIsUpdate] = useState(false);

  useEffect(() => {
    if (typeof window !== "undefined")
    {
      if (localStorage.getItem("token") === null)
      {
        alert('Please signup/login first!');
        router.push('/');
      }
      else
      {
        axios.post('http://localhost:5000/api/token_login_user/', { token:  localStorage.getItem("token") })
        .then(res => {
          if (res.status == 200)
          {
            if (typeof window !== "undefined")
            {
              setUser(res.data);
              router.push('/home');
            }
          }
          else 
          {
            if (typeof window !== "undefined")
            {
              localStorage.removeItem('token');
              router.push('/');
            }
          }
        })
        .catch(err => console.log(err));
        axios.get('http://localhost:5000/api/get_users/')
          .then(res => setUsers(res.data))
          .catch(err => console.log(err));
      }
    }
  }, []);

  return (
    <div>
      {user !== null ?
        <>
          <Head>
            <title>Simple Web Api | Home</title>
          </Head>

          <Navbar setIsUpdate={setIsUpdate} user={user} page="/home"/>
          {!isUpdate ?
            <UsersList user={user} users={users} setUsers={setUsers}/> :
            <SignUpForm user={user} page="/home" setUser={setUser}/>
          }
        </> :
        <></>}
    </div>
  );
};

export default index;
